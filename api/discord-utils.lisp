;;;; this file defines all the utility functions that will be used in the :discord-lisp.api package.
;;;; i am putting them in a separate file in order to archieve a much more clean source structuring.
(defpackage :discord.utils
  (:use :cl)
  (:export
   :pair->pair-string
   :alist->query-string))
(in-package :discord.utils)

;;; this one is gonna be used to process a cons cell pair into a key=value string suitable for usage
;;; as HTTP request query parameters; an example could be a cons cell defined as (:KEYWORD t), it could
;;; be transformed into "keyword=true" according to my function definition.
(defun pair->pair-string (cons)
  (labels ((to-string (object)
	     (cond
	       ((numberp object) (write-to-string object))
	       ((stringp object) object)
	       ((keywordp object) (string-downcase (string object)))
	       ((eq t object) "true")
	       ((eq nil object) "false")
	       (t (error "Tried to process an object that is not a valid query parameter.")))))
    (let ((key (car cons)) (value (cdr cons)))
      (concatenate 'string (to-string key) "=" (to-string value)))))

;;; this one processes an associative list & returns an string that is composed of a bunch of key=value
;;; pairs suitable for HTTP query parameter passing, separated by an "&", returns nil if the alist is nil;
;;; an example would be this: '((:KEYWORD t) ("query" nil)), this could become "keyword=true&query=false".
(defun alist->query-string (alist)
  (unless (null alist)
    (let ((query-string "?"))
      (do
       ((element alist (cdr element)))
       ((null element) query-string)
	(setf query-string (concatenate 'string
					query-string
					(pair->pair-string (car element))
					(unless (null (cdr element))
					  "&")))))))
