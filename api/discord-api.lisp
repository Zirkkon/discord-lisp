;;;; definition of the discord functions that shall be used for all the current project source
;;;; files, this file contains the functions that will be used to interface with the HTTP discord
;;;; Application Programming interface, so here will not be too much code in itself.
(defpackage discord.api
  (:use :cl)
  (:export
   :*library-version*
   :*rest-version*
   :*default-user-agent*
   :*default-rest-uri*
   :*global-credentials*
   :credentials
   :make-credentials
   :set-global-credentials
   :request))
(in-package :discord.api)

;;; the version of the discord library.
(defvar *library-version* "0.1.0")

;;; the version of the discord REST API we will be using/requesting to.
(defvar *rest-version* "7")

;;; the default user agent we use.
(defvar *default-user-agent* (format nil "DiscordBot (~A, v~A.~A)"
				 "https://gitlab.com/Zirkkon/discord-lisp"
				 *rest-version* *library-version*))

;;; the default requests URI we will be using from discord.
(defvar *default-rest-uri* (format nil "https://discord.com/api/v~A" *rest-version*))

;;; the credentials we will use to authenticate with the discord API.
(defvar *credentials* nil)

;;; makes keeping track of the credential itself be easier.
(defclass credentials ()
    ((token
      :type string
      :accessor token
      :initarg :token
      :initform (error "A token is required to initialize the credentials object."))
     (auth-type
      :type string
      :accessor auth-type
      :initarg :auth-type
      :initform "Bot")))

;;; to make a credential & let it be used with with-credentials.
(defun make-credentials (&key token auth-type)
  (make-instance 'credentials :token token :auth-type auth-type))

;;; what we will use to make a header value valid to authorize with the thing.
(defun format-credentials (credentials)
  (format nil "~A ~A"  (auth-type credentials) (token credentials)))

;;; dynamic local scope credentials, for a "lispier" interfacing.
(defmacro with-credentials (credentials &body body) 
  (alexandria:with-unique-names (the-credentials)
    (let ((the-credentials credentials))
      `(let ((*credentials* (format-credentials ,the-credentials)))
	 ,@body))))

;;; the request function definition we will actually be using, it is a small wrapper around the
;;; dexador request function.
(defun bare-request (url method content more-headers-plist &rest parameters)
  (apply #'dex:request url
	 :method method
	 :headers (alexandria:plist-alist (list* "User-Agent" *default-user-agent*
						 "Authorization"
						 *credentials*
						 more-headers-plist))
	 :content content
	 parameters))

;;; small wraper to have all the .json output from the request to become lisp lisps.
(defun request (url method content more-headers-plist &rest parameters)
  (cl-json:decode-from-string (bare-request url method content more-headers-plist parameters)))
