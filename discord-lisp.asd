(defsystem "discord-lisp"
  :version "0.1.0"
  :author "Joshua Pc. Hdz."
  :license ""
  :depends-on (:dexador
               :cl-json
               :alexandria)
  :components ((:module "api"
                :components
                ((:file "discord-utils")
		 (:file "discord-api" :depends-on ("discord-utils")))))
  :description "A common lisp library intented to write bots for the discord API"
  :long-description
   #.(read-file-string
     (subpathname *load-pathname* "README.org")))
